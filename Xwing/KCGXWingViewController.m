//
//  KCGXWingViewController.m
//  Xwing
//
//  Created by Fernando Rodríguez Romero on 12/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import "KCGXWingViewController.h"

@interface KCGXWingViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *spaceView;
@property (weak, nonatomic) IBOutlet UIImageView *xwingView;

@end

@implementation KCGXWingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Creamos en reconocedor de Taps
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    
    // Lo encasquetamos a self.spaceView
    [self.spaceView addGestureRecognizer:tap];
    
}

-(void) tap: (UITapGestureRecognizer *) recognizer{
  
    if (recognizer.state == UIGestureRecognizerStateRecognized) {
        
        UIViewAnimationOptions options = UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState;
        
        // Translación
        [UIView animateWithDuration:1.0
                              delay:0.0
                            options:options
                         animations:^{
                             self.xwingView.center = [recognizer locationInView:self.spaceView];
                         } completion:^(BOOL finished) {
                             //
                         }];
        
        
        // Rotación
        [UIView animateWithDuration:0.5
                              delay:0
                            options:options
                         animations:^{
                             self.xwingView.transform = CGAffineTransformMakeRotation(M_PI_2);
                             
                         } completion:^(BOOL finished) {
                             //
                             [UIView animateWithDuration:0.5
                                                   delay:0
                                                 options:options
                                              animations:^{
                                                  self.xwingView.transform = CGAffineTransformIdentity;
                                                  
                                                  
                                              } completion:^(BOOL finished) {
                                                  //nada
                                              }];
                         }];
        
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
