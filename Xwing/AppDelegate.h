//
//  AppDelegate.h
//  Xwing
//
//  Created by Fernando Rodríguez Romero on 12/01/16.
//  Copyright © 2016 keepcoding.io. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

